Descripción
===

Docker compose que permite la orquetación y ejecución de pruebas de carga sobre meli  proxy

Requisitos
===

Contar con docker compose instalado
Tener MAKE instalado

Ejecución
===

1.- Docker compose build de los compoenentes

```
$ docker compose build
```

2.- Levantar proxy meli 

```
$ make proxy-meli-go-up
```

Ejecución de pruebas
===

1.- Tener corriendo el proxy meli

2.- Ejecutar las pruebas

```
$ make bench-first-up
```


