.PHONY: about
about:
	@echo "Carga de componentes para meli proxy"

proxy-meli-go-run:
	docker compose run --service-ports proxy-meli-go bash

proxy-meli-go-up: mongo-db-up mocks-up
	docker compose stop  proxy-meli-go
	docker compose up --remove-orphans proxy-meli-go

mongo-db-up:
	docker compose stop mongo-db
	docker compose up -d --remove-orphans mongo-db
mocks-up:
	docker compose stop mocks
	docker compose up -d --remove-orphans mocks
bench-first-up:
	docker compose stop bench-first
	docker compose up --remove-orphans bench-first